DOCKER_COMPOSE = COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose
DOCKER_RUN = ${DOCKER_COMPOSE} run -u `id -u`
DOCKER_BUILD = DOCKER_BUILDKIT=1 docker build
DOCKER_CONTAINER_NAME = backend
MANAGE_EXEC = python /app/src/manage.py
MANAGE_COMMAND = ${DOCKER_RUN} ${DOCKER_CONTAINER_NAME} ${MANAGE_EXEC}

init: setup_venv pre_commit_install migrate

run:
	${DOCKER_COMPOSE} up

setup_venv:
	rm -rf venv
	python3.11 -m venv venv;
	venv/bin/python -m pip install wheel setuptools;
	venv/bin/python -m pip install pre-commit boto3 pip-tools;

pre_commit_install:
	venv/bin/pre-commit install

pre_commit_run_all:
	venv/bin/pre-commit run --all-files

makemigrations:
	${MANAGE_COMMAND} makemigrations ${ARGS}

migrate:
	${MANAGE_COMMAND} migrate ${ARGS}

createsuperuser:
	${MANAGE_COMMAND} createsuperuser

shell:
	${MANAGE_COMMAND} shell

manage_command:
	${MANAGE_COMMAND} ${ARGS}

add_dependency:
	${DOCKER_RUN} ${DOCKER_CONTAINER_NAME} poetry add --lock ${DEPENDENCY}

add_dev_dependency:
	${DOCKER_RUN} ${DOCKER_CONTAINER_NAME} poetry add -D --lock ${DEPENDENCY}

poetry_lock:
	${DOCKER_RUN} ${DOCKER_CONTAINER_NAME} poetry lock --no-update

poetry_command:
	${DOCKER_RUN} ${DOCKER_CONTAINER_NAME} poetry ${COMMAND}

build_dev_docker_image: compile_requirements
	${DOCKER_COMPOSE} build ${DOCKER_CONTAINER_NAME}

compile_requirements:
	./venv/bin/pip-compile --output-file requirements/base.txt requirements/base.in
	./venv/bin/pip-compile --output-file requirements/test.txt requirements/test.in
	./venv/bin/pip-compile --output-file requirements/dev.txt requirements/dev.in
