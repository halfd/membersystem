# member.data.coop

## Development

### Setup environment

Copy over the .env.example file to .env and adjust DATABASE_URL accordingly

        $ cp .env.example .env

### Docker

#### Requirements

- Docker
- Docker compose
- pre-commit (preferred for contributions)

#### Setup

Given that the requirements above are installed, it should be as easy as:

    $ make migrate

This will setup the database. Next run:

    $ make run

This will build the docker image and start the member system on http://localhost:8000.

You can create a superuser by running:

    $ make createsuperuser

Make migrations:

    $ make makemigrations

Make messages:

    $ make makemessages

Running tests:

    $ make test

### Non-docker

Create a venv

        $ python3 -m venv venv

Activate the venv

        $ source venv/bin/activate

Install requirements

        $ pip install -r requirements/dev.txt

Run migrations

        $ ./src/manage.py migrate

Create a superuser

        $ ./src/manage.py createsuperuser

Run the server

        $ ./src/manage.py runserver
