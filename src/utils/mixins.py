from django.db import models
from django.utils.translation import gettext_lazy as _


class CreatedModifiedAbstract(models.Model):
    modified = models.DateTimeField(auto_now=True, verbose_name=_("modified"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("created"))

    class Meta:
        abstract = True
