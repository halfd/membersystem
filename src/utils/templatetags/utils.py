from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def active_path(context, path_name, class_name) -> str | None:
    """Return the given class name if the current path matches the given path name."""

    path = reverse(path_name)
    request_path = context.get("request").path

    # Check if the current path matches the given path name.
    is_path = path == request_path

    # Check if the current path is a sub-path of the given path name.
    is_base_path = "base_path" in context and reverse(context["base_path"]) == path

    if is_path or is_base_path:
        return class_name
