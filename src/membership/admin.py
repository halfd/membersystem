from django.contrib import admin

from .models import Membership
from .models import MembershipType
from .models import SubscriptionPeriod


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    pass


@admin.register(MembershipType)
class MembershipTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(SubscriptionPeriod)
class SubscriptionPeriodAdmin(admin.ModelAdmin):
    pass
