from dataclasses import dataclass

from django.contrib.auth.models import Permission as DjangoPermission
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _

PERMISSIONS = []


def persist_permissions(sender, **kwargs):
    for permission in PERMISSIONS:
        permission.persist_permission()


@dataclass
class Permission:
    name: str
    codename: str
    app_label: str
    model: str

    def __post_init__(self, *args, **kwargs):
        PERMISSIONS.append(self)

    @property
    def path(self):
        return f"{self.app_label}.{self.codename}"

    def persist_permission(self):
        content_type, _ = ContentType.objects.get_or_create(
            app_label=self.app_label,
            model=self.model,
        )
        DjangoPermission.objects.get_or_create(
            content_type=content_type,
            codename=self.codename,
            defaults={"name": self.name},
        )


ADMINISTRATE_MEMBERS = Permission(
    name=_("Can administrate members"),
    codename="administrate_members",
    app_label="membership",
    model="membership",
)
