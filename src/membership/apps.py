from django.apps import AppConfig
from django.db.models.signals import post_migrate


class MembershipConfig(AppConfig):
    name = "membership"

    def ready(self):
        from .permissions import persist_permissions

        post_migrate.connect(persist_permissions, sender=self)
