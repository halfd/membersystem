from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from . import models


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("who", "description", "created", "is_paid")

    @admin.display(description=_("Customer"))
    def who(self, instance):
        return instance.user.get_full_name()


@admin.register(models.Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ("who", "description", "order_id", "created")

    @admin.display(description=_("Customer"))
    def who(self, instance):
        return instance.order.user.get_full_name()

    @admin.display(description=_("Order ID"))
    def order_id(self, instance):
        return instance.order.id
