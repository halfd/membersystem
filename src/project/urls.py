"""URLs for the membersystem"""
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include
from django.urls import path

from .views import index
from .views import services_overview
from membership.views import members_admin
from membership.views import members_admin_detail
from membership.views import membership_overview

urlpatterns = [
    path("", login_required(index), name="index"),
    path("services/", login_required(services_overview), name="services"),
    path("membership/", membership_overview, name="membership-overview"),
    path("admin/members/", members_admin, name="admin-members"),
    path(
        "admin/members/<int:member_id>/",
        members_admin_detail,
        name="admin-members-detail",
    ),
    path("accounts/", include("allauth.urls")),
    path("_admin/", admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = [
        path("__debug__/", include("debug_toolbar.urls")),
        path("__reload__/", include("django_browser_reload.urls")),
    ] + urlpatterns
